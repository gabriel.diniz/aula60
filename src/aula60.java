public class aula60 {  // manipulando planilhas

    public static void main(String[] args) {

        double[][] plan = // array plan tipo double
                {
                        {434, 45, 912, 42, 54, 0},
                        {23.8, 973.3, 254.8, 744.2, 3.74, 0},
                        {23.8, 973.3, 254.8, 744.2, 3.74, 0},
                        {53.52, 67, 873, 245.9, 54.12, 0},
                        {234.25, 54.2, 82.0, 745.2, 82, 0}
                };

        double somaTotal = 0;  // variavel somatotal valor 0
        // laço de repetição
        for (int x = 0; x < 5; x++) { // variavel inteira x valor 0, x menor que 5, cada ciclo acrescenta 1 unidade
            for (int y = 0; y < 5; y++) { // variavel inteira y valor 0, y menor que 5, cada ciclo acrescenta 1 unidade
                plan[x][5] += plan[x][y];  //  variavel plan posição x incrementa valor, igual plan x y
            }
            somaTotal += plan[x][5]; // somatotal mais igual valor de x quinta posição, x valor da linha, 5 valor da coluna
        }
        // laço de repetição
        for (int x = 0; x < 5; x++) { // variavel inteira x valor 0, y menor que 5, cada ciclo acrescenta 1 unidade
            String str = ""; // variavel tipo string str, valor vazio
            for (int y = 0; y < 6; y++) { // y igual 0, y menor que 6, cada ciclo acrescenta 1 unidade
                if (y < 5) // se y menor que 5
                    str += plan[x][y] + ", "; //str mais igual plan x y ,
                else // se não
                    str += plan[x][y]; //str mais igual plan x y
            }
            System.out.println(str); // imprimir valor str
        }
        System.out.println("A soma total é: "+somaTotal); // imprime a soma total
    }
}